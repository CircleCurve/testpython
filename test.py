#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#print statement
print("hello, world")
print("The quick brown fox", "jump over", "the lazy dog")
print(300 + 200)
print(100)
print("100+200=", 100 + 200)
print("1024x768=", 1024 * 768)

#print line
print('''line1
line2
line3''')

#PI
print("PI",10/3)

#practise
n = 123
f = 456.789
s1 = 'Hello, World'
s2 = 'Hello, \'Adam\''
s3 = r'Hello, "Bart"'
s4 = r'''Hello,
Lisa!!'''

print("Start practise")
print(n)
print(f)
print(s1)
print(s2)
print(s3)
print(s4)
print("End practise")


#type chinese
print('包含中文的str')
print("A=", ord('A'))
print("中=", ord('中'))
print('\u4e2d\u6587')
x = b'ABC'
print('x=', x)
print("ABC".encode("ascii"))
print('中文'.encode('utf-8'))
print("ABC decode :", b'ABC'.decode('ascii'))
print("中文 decode :", b'\xe4\xb8\xad\xe6\x96\x87'.decode('utf-8'))
print('len of ABC', len('ABC'))
print('len of 中文', len('中文'))

arr = ['world',2]
print('Hello,%s , %s' % tuple(arr))

#practise of caculator
print("Start practise of caculator ")
s1 = 72
s2 = 85
r = ((85-72)/72) * 100
print('Percentage is %.2f %%' % r)
print("End practise of caculator ")

#practise of List tuple
print("Start list tuple")
classmates = ['Michael', 'Bob', 'Tracy']
print('Classmates', classmates)
print('Classmates len :', len(classmates))
classmatesStr = 'last index of classmates'+ classmates[-1]+ ' last second index :'+ classmates[-2]
print(classmatesStr)

classmates.append('Adam')
print('classmates append' , classmates)

classmates.insert(1, 'Jack')
print('classmates insert jack : ', classmates)

classmates.pop()
print('classmates pop of last index', classmates)

#tuple
classmates = ('Michael', 'Bob', 'Tracy')
print('Tuple classmates :' , classmates)

t = () #define tuple ()  list = []
print ('t tuple :', t , " length:" ,len(t))


L = [
    ['Apple', 'Google', 'Microsoft'],
    ['Java', 'Python', 'Ruby', 'PHP'],
    ['Adam', 'Bart', 'Lisa']
]

#apple
print('Apple : ' , L[0][0])

#Python:
print('Python :', L[1][1])

#Lisa:
print('Lisa : ', L[2][2])


print("End list tuple")




# if case implements ;
a = -1
if a >= 0:
    print("a=", a)
else:
    print(a)

#input file
name = input("Please input your name :  ")
print("Your name is :", name, " ", 1)
